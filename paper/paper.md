---
title: 'VibIR-Parallel-Compute: Enhancing Vibration and Infrared Analysis in High-Performance Computing Environments'
tags:
  - Python
  - material science
  - vibrational analysis
  - infrared spectra
authors:
  - name: Kurt Irvin M. Rojas
    orcid: 0000-0002-2780-0484
    #equal-contrib: true
    corresponding: true
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Yoshitada Morikawa
    orcid: 0000-0003-4895-4121
    affiliation: "1, 2"
  - name: Ikutaro Hamada
    orcid: 0000-0001-5112-2452
    affiliation: 1
affiliations:
 - name: Department of Precision Engineering, Graduate School of Engineering, Osaka University, 2-1, Yamadaoka, Suita, Osaka, 565-0871, Japan
   index: 1
 - name: Research Center for Precision Engineering, Graduate School of Engineering, Osaka University, 2-1, Yamadaoka, Suita, Osaka, 565-0871, Japan
   index: 2
date: 22 August 2024
bibliography: paper.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
# aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it.
# aas-journal: Astrophysical Journal <- The name of the AAS journal.
---

# Summary

In this work,  we introduce `VibIR-Parallel-Compute`, a Python package designed to enhance both the usability and efficiency of vibrational and infrared (IR) analysis, particularly for high-performance computing environments. To address the computational bottleneck encountered in the vibrational spectroscopy modules in `Atomic Simulation Environment` (ASE) [@hjorthlarsenAtomicSimulationEnvironment2017], we provide a *drop-in* replacement that parallelizes these bottleneck calculations in the current implementation. Additionally, we offer customizable workflows for processing the data required for vibrational and IR analysis, enabling previously incompatible calculators to now be compatible. Furthermore, our package includes streamlined and efficient analysis modules that allow for quick visualization of vibrational modes, along with the plotting of vibrational density of states (VDOS) and IR spectra, all without the need to explore the underlying source code or manage excessive boilerplate.


# Statement of need

Vibrational spectroscopy has become a valuable technique for analyzing structures of molecules and materials [@krakaDecodingChemicalInformation2020]. IR spectroscopy, in particular, is widely utilized to examine the inherent strength of chemical bonds and the characteristic bonding patterns present in materials. However, assigning vibrational modes to experimental spectra is challenging and typically requires comparison with model spectra predicted through electronic structure calculations, such as density functional theory (DFT). 

A commonly used and efficient method for determining vibrational characteristics, such as modes and frequencies, involves solving for the eigenvector and eigenvalues of a Hessian matrix [@wilsonMolecularVibrationsTheory1980;@giustinoMaterialsModellingUsing2014]. This matrix represents the strength of chemical bonds under the harmonic approximation. Each element in the Hessian matrix is defined as:

\begin{equation} \label{eq:h_elem}
H_{ij} = - \frac{\partial F_j}{\partial u_i}  
\end{equation}

where the Hessian element $H_{ij}$ reflects the negative gradient of the force $F_j$ with respect to atomic position $u_i$. The $i$ and $j$ span the three spatial coordinates across $N$ atoms.

The force gradient is determined using the finite-difference method, which involves applying small forward and backward displacements to atoms. This requires calculating the forces twice -- once for the forward displacement and once for the backward displacement -- both of which are independent computations. This independence is crucial as it enables parallel force evaluations for different displaced structure. 

The construction and diagonaliation of the Hessian matrix are implemented in the widely-used Python package, ASE. ASE offers various methods and workflow that interface with several *ab initio* calculators, such as VASP [@kresseInitioMolecularDynamics1993a], Quantum Espresso [@giannozziQUANTUMESPRESSOModular2009a], and GPAW [@mortensenGPAWOpenPython2024] . Modules related to spectroscopy, specifically, can be found in the `ase.vibrations` module. In ASE's implementation, the total number force evaluations $N_{\textrm{calc}}$ required by a system with $N_{\textrm{atom}}$ atom is given by:

\begin{equation} \label{eq:n_calc}
N_{\textrm{calc}} = 6 \cdot N_{\textrm{atom}} + 1
\end{equation}

In this equation, the factor of 6 accounts for the three spatial directions, considering both forward and backward displacements. The additional $+1$ term is for the equilibrium structure. This total number of calculations ensures compatibility with all three finite-difference methods: central, forward, and backward.

Although parallelizing the calculations for the displaced systems is straightforward, the current implementation runs in a serial loop with a time complexity of $\mathcal{O}(N_{\textrm{calc}})$. This limits the ability to take full advantage of the large computing resources typically available in high-performance computing (HPC) environments. While parts of the existing code could be adapted (along with new compatibility code) to implement parallelization, doing so would increase complexity and reduce the clarity of the script-level codes. Therefore, a more user-friendly, parallelized version of the `ase.vibrations` toolkit is desirable.

Another issue arises from the need for an additional calculation input and output processing layer. When generating IR spectra, we require the dipole moment changes of the system. In a three-dimensional system, such as an isolated molecule, this involves calculating a three-dimensional dipole moment vector. Unfortunately, some calculators -- such as Quantum Espresso -- only computes and outputs the dipole moment as a scalar along the chosen electric field direction (typically along the x, y, and z axis). This means that three separate calculations are necessary to obtain the full dipole moment vector. Currently, ASE does not support adding a processing layer capable of handle these complex but necessary workflows.  

# Features and Implementation



## Parallel ready code

In this work, we focused on enhancing the existing code to support parallelization by distributing each independent atomically displaced atomic system across separate computing resources. This approach, in theory, allows all systems to run simultaneously. We leveraged resource workload managers commonly found in high-performance computing environments (e.g. PBS, SLURM, SGE) to efficiently assign each system to a distinct resource pool.

![Workflow comparison. The serial and parallel workflow are compared, with the key difference being how resources are allocated for each displaced structure. In the structure/forces section, "$eq$" refers to the equilibrium structure, while "$1x+$" denotes atom 1 forward displced along x-axis. \label{fig:workflow}](workflow.png){ width=90% }

\autoref{fig:workflow} compares the workflows of the serial and parallel implementations. In the serial version, all structures are assigned to a single resource pool, while in the parallel version, each structure is distributed to a separate pool, enabling simultaneous calculations. The serial loop relies on the `run()` method, but we modified the code to support parallelization by adopting a module approach. We introduced a `run_tag(i)` method to run individual systems based on their index which can be executed using job arrays in resource workload managers. The following is a pseudo code for the difference between the serial and parallelized version:


>```python
># serial
>vib = Vibrations(atoms)
>vib.run()
>
># parallel - in serial loop
>pvib = ParallelVibrations(atoms, calc_obj=EMT)
>for i in range(1, vib.map_length +1):
>    pvib.run_tag(i)
>
># parallel - parallelized
>pvib = ParallelVibrations(atoms, calc_obj=EMT)
>pvib.run_tag(int(os.environ['PBS_ARRAY_INDEX']))
>```

### Timings
The timing results in \autoref{fig:timing} illustrate the distinct complexities associated with each implementation scenario. 
In the serial implementation, the time complexity is $\mathcal{O}(N_{\textrm{calc}})$.
By contrast, in a fully parallelized version -- where all needed resources are available -- it reduces to $\mathcal{O}(1)$. 
However, in practice, this ideal scenario is rarely achieved due to HPC fair-use policies that limit the number of resources a user can access simultaneously. 
This limited-resource strategy can be referred to as *batch parallel*, where jobs are grouped into resource-constrained batches to meet fair-use requirements.
The third case in the figure illustrates a batch parallel scenario with a fair-use limit of 10 simultaneous jobs, leading to an effective time complexity of $\mathcal{O}(\lceil N_{\textrm{calc}}/10 \rceil)$. 

![Example timings. We present two cases to demonstrate how parallelization reduces overall real-time spent. Additionally, a batch parallel run is considered to highlight the impact of fair-use policies on parallelization timings. \label{fig:timing}](timings.png){ width=90% }

### Temporary files issue

One downside of this parallelized approach is the generation of numerous temporary files from the calculations, which can be quite large (e.g. charge density, wavefunction data). On larger systems, this could risk exhausting disk space. To address this, we implemented a file management system that allows deletion of temporary files after the calculations are complete. Additionally, since multiple calculations run in parallel, we created a system that ensures each calculation operates in a separate working directory, preventing any accidental sharing or overwriting of output and temporary files. 

## Processing layer

In this additional processing layer, we designed the calculation to be flexible, allowing for more complex workflows. Our focus was on the applicability of Quantum Espresso for infrared spectra calculations. As mentioned earlier, Quantum Espresso only calculates the dipole moment in a single direction. However, for molecular, ribbon, and sheet systems, we require three-dimensional, two-dimension, and one-dimensional dipole moment vectors, respectively. To handle this, we implemented a three-step calculation for each dipole direction and constructed the full dipole moment vector from these results. Additionally, for ribbon and sheet sytems, we skip calculating the dipole moment along periodic directions to save time and resources. A schematic of the processing layer is shown in \autoref{fig:compat_layer}. 

![Compatability layer. We present the diagram of the general compatability layer and a specific example of the Quantum Espresso compatability layer. \label{fig:compat_layer}](compat_layer.png){ width=100% }


## Fast analysis

While ASE provides tools for analyzing vibrational modes and frequencies, we enhanced its user-friendliness. We introduced a quick-plot feature for vibrational modes and frequencies in both Vibrations (\autoref{fig:vdos}) and Infrared (\autoref{fig:infrared})analyses. Additionally, we implemented a more detailed GIF animation of the vibrational modes to offer a clearer, more intuitive understanding. To improve efficiency, we applied CPU-based parallelization for GIF generation, significantly speeding up the process.

![Vibrational density of states of water. \label{fig:vdos}](vdos_spectra_water.png){ width=80% }

![Simulated infrared spectra of water. \label{fig:infrared}](infrared_spectra_water.png){ width=80% }

![Static representation of mode GIFs. The image includes mode 6, 7 and 8 of water      as an example. \label{fig:mode_combined}](mode_combined.png){ width=60% }

# Usage

The `VibIR-Parallel-Compute` package is available on Gitlab https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute and documentations are provided in https://morikawa-lab-osakau.gitlab.io/vibir-parallel-compute. 

<!-- 
# Citations

Citations to entries in paper.bib should be in
[rMarkdown](http://rmarkdown.rstudio.com/authoring_bibliographies_and_citations.html)
format.

If you want to cite a software repository URL (e.g. something on GitHub without a preferred
citation) then you can do it with the example BibTeX entry below for @fidgit.

For a quick reference, the following citation commands can be used:
- `@author:2001`  ->  "Author et al. (2001)"
- `[@author:2001]` -> "(Author et al., 2001)"
- `[@author1:2001; @author2:2001]` -> "(Author1 et al., 2001; Author2 et al., 2002)"

# Figures

Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

Figure sizes can be customized by adding an optional second parameter:
![Caption for example figure.](figure.png){ width=20% }

# Acknowledgements

We acknowledge contributions from Brigitta Sipocz, Syrtis Major, and Semyeong
Oh, and support from Kathryn Johnston during the genesis of this project. -->

# References

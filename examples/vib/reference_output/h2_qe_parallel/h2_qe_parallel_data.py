from ase.build import molecule
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelVibrations
from pathlib import Path
import sys


atoms = molecule("H2", cell=[10, 10, 10])
atoms.center()

# Set the calculator object
calc_obj = Espresso

# Set calculator object kwargs
outdir = "TMPDIR"
calc_kwargs = {
    "profile": EspressoProfile(
        command="mpirun pw.x",
        pseudo_dir=Path(".").resolve(),
    ),
    "input_data": {
        "control": {
            "tprnfor": True,
            "outdir": outdir,
        },
        "system": {
            "ecutwfc": 50,
            "ecutrho": 400,
        },
    },
    "pseudopotentials": {"H": "h_pbe_v1.4.uspp.F.UPF"},
    "directory": "DFT",
}


# Initialize ParallelVibrations
vib = ParallelVibrations(
    atoms,
    name="H2_qe_parallel",
    calc_obj=calc_obj,
    calc_kwargs=calc_kwargs,
)

# Run a single displacement (Parallelization is through job arrays)
ARRAY_ID = int(sys.argv[1])
vib.run_tag(ARRAY_ID)

FROM python:3.10.14-slim-bookworm

ENV DEBIAN_FRONTEND=noninteractive
SHELL ["/bin/bash", "-c"]

RUN apt update -y && \
    apt install -y --no-install-recommends build-essential ca-certificates \
                   gfortran libblas3 libc6 libfftw3-dev libgcc-s1 liblapack-dev  \
                   wget git gosu cmake

# ------ USER ENVIRONMENT ------
ENV USER=testuser
RUN adduser --disabled-password --gecos "" ${USER} && \
    echo "${USER} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER ${USER}
WORKDIR /home/${USER}

# ------ QE ENVIRONMENT ------
ENV QESRC=/home/${USER}/qe/src
ENV QEBIN=/home/${USER}/qe/bin
RUN mkdir -p ${QEBIN}

RUN wget https://gitlab.com/QEF/q-e/-/archive/qe-7.2/q-e-qe-7.2.tar.gz && \
    # tar to QESRC
    tar zxvf q-e-qe-7.2.tar.gz && \
    mv q-e-qe-7.2 ${QESRC} && \
    rm q-e-qe-7.2.tar.gz

# RUN git clone --depth 1 --branch qe-7.2 https://gitlab.com/QEF/q-e.git ${QESRC}
WORKDIR ${QESRC}
RUN ./configure --disable-parallel && \
    make pw -j4 && \
    cp bin/* ${QEBIN}/
# RUN mkdir build && cd build && \
#     # cmake -DCMAKE_INSTALL_PREFIX=${QEBIN} -DQE_ENABLE_MPI=OFF -DQE_ENABLE_STATIC_BUILD=ON ../ && \
#     cmake -DCMAKE_INSTALL_PREFIX=${QEBIN} -DQE_ENABLE_MPI=OFF ../ && \
#     make pw -j4 && \
#     cp bin/* ${QEBIN}/

WORKDIR /home/${USER}
# RUN rm -rf ${QESRC}

# ------ PSEUDO ENVIRONMENT ------
ENV QEPSEUDO=/home/${USER}/qe/pseudo
WORKDIR ${QEPSEUDO}
RUN wget https://www.physics.rutgers.edu/gbrv/all_pbe_UPF_v1.5.tar.gz && \
    tar zxvf all_pbe_UPF_v1.5.tar.gz && \
    rm all_pbe_UPF_v1.5.tar.gz


WORKDIR /home/${USER}

# ------ PYTHON ENVIRONMENT ------
RUN python -m venv pyenv && \
    source pyenv/bin/activate && \
    pip install -U pip && \
    pip install tqdm ase==3.23 seaborn pytest

CMD ["bash"]


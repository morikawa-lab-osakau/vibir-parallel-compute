# JOSS Requirements

The following is a checklist of the requirements for the JOSS submission.

## Submission requirements

1. The software must be open source as per the OSI definition.
   : The software follows the OSI definition
2. The software must be hosted at a location where users can open issues and propose code changes without manual approval of (or payment for) accounts.
   : The software is hosted on GitLab
3. The software must have an obvious research application.
   : The software is readily applicable to research specfically on computing and data analysis of vibrations and infrared data from densition funcitonal theory (DFT) calculations. This software was also used for a research paper under preparation. 
4. You must be a major contributor to the software you are submitting, and have a GitHub account to participate in the review process.
   : I (the submitter) am the major contributor of this project. I have a GitLab account to participate in the review process. Contributor analytics are available [here](https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute/-/graphs/joss_paper).
5. Your paper must not focus on new research results accomplished with the software.
   : The JOSS submission focuses on the software itself, not on new research results.
6. Your paper (paper.md and BibTeX files, plus any figures) must be hosted in a Git-based repository together with your software.
   : The paper is hosted in the same repository (`joss_paper` branch) as the software. The softare is hosted in GitLab, a Git-based repository.
7. The paper may be in a short-lived branch which is never merged with the default, although if you do this, make sure this branch is created from the default so that it also includes the source code of your submission.
   : Same as above.

Additionally, the software is:
- stored in a repository that can be cloned without registration.
- stored in a repository that is browsable online without registration.
- has an issue tracker that is readable without registration
- allows individuals to create issues/file tickets against your repository.

## Substantial scholarly effort checklist

- Age of software
  : The initial commit of the repository started on June 2024. The history analytics can be found [here](https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute/-/commits/joss_paper). The official start was June 2024, however, internal developments started since [December 2024](https://github.com/kimrojas/MatSciToolKit/commits/main/matscitoolkit/ase_vib_tools.py)
- Number of commits
  : There are 60 commits as of the time of submission. The commit history can be found [here](https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute/-/value_stream_analytics?created_after=2024-06-01&created_before=2024-08-22&stage_id=issue&sort=duration&direction=desc&page=1). Note that commits were squashed when issue branches were merged into the main branch, hence a lower number of commits are shown.
- Number of authors
  : The number of authors of the JOSS submission is 3. 
- Total lines of code (LOC). Submissions under 1000 LOC will usually be flagged, those under 300 LOC will be desk rejected.
  : The LOC is 4174 using the following code.
    ```bash 
    git ls-files --exclude-standard -- ':!:**/*.[pjs][npv]g' ':!:**/*.UPF' ':!:**/*.o*' ':!:**/*.traj' ':!:**/*.gif' ':!:**/*.json' ':!:package-lock.json' ':!:**/*.pwo' ':!:**/*.pwi' ':!:**/*.err' ':!:**/*.xsf' | xargs wc -l
    ```
- Whether the software has already been cited in academic papers.
  : Not yet. The research paper under preparation will cite this software.
- Whether the software is sufficiently useful that it is likely to be cited by your peer group.
  : This software is likely to be cited by our group and other researchers doing similar compuations. The vibrations and infared functionally is really useful in the field of computational chemistry and materials science, and this software makes it easier to use and analyze the data.

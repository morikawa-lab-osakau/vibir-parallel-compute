#!/bin/bash
#PBS -q DBG
#PBS -N h2_qe_parallel_analysis
#PBS --group=G15577
#PBS -l elapstim_req=00:10:00
#PBS -l cpunum_job=76
#PBS -v OMP_NUM_THREADS=1
#PBS -T intmpi
#PBS -b 1
#PBS -j o

cd $PBS_O_WORKDIR

# -- ENVIRONMENT --------------------------------------------------------------
module load BaseCPU/2024
source ~/work/apps/pyenv/vibirdev/bin/activate
ulimit -s unlimited

export PATH=/sqfs/home/u6c301/work/apps/qe/q-e-qe-7.2/bin:$PATH

# -- EXECUTION ----------------------------------------------------------------
echo "========= Job started  at `date` =========="

python -u h2_qe_parallel_analysis.py

echo "========= Job finished at `date` =========="

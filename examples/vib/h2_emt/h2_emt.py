from ase.io import read, write
from ase.build import molecule
from ase.calculators.emt import EMT
from ase.optimize import BFGS
from vibir_parallel_compute import ParallelVibrations, ParallelInfrared


atoms = molecule("H2", cell=[10, 10, 10])
atoms.center()

# Set the calculator object
calc_obj = EMT

# Set calculator object kwargs
calc_kwargs = {}

# Initialize ParallelVibrations
vib = ParallelVibrations(
    atoms,
    name="H2_emt",
    calc_obj=calc_obj,
    calc_kwargs=calc_kwargs,
)

# Run a serial computation (This can be done in parallel via arrays)
for i in range(1, vib.map_length + 1):
    vib.run_tag(i)
vib.run()

vib.summary()

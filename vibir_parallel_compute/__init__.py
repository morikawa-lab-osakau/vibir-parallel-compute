from .parallel.vibrations import ParallelVibrations
from .parallel.infrared import ParallelInfrared


__version__ = "1.0.0"

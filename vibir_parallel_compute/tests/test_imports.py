from vibir_parallel_compute import ParallelVibrations, ParallelInfrared


def test_imports():
    assert ParallelVibrations
    assert ParallelInfrared

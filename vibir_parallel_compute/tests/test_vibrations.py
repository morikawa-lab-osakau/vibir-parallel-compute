from vibir_parallel_compute import ParallelVibrations
from vibir_parallel_compute.tests.aseconfig import *


@pytest.mark.parametrize("calc_obj", [EMT, Espresso])
@pytest.mark.parametrize("name", ["H2", "N2"])
def test_workflow(name, calc_obj):
    atoms = molecule(name, cell=[5, 5, 5])
    calc_name = calc_obj.__name__
    if calc_name == "EMT":
        calc_kwargs = {}
    if calc_name == "Espresso":
        calc_kwargs = {
            "input_data": input_data_template,
            "profile": espresso_profile,
            "kpts": (1, 1, 1),
            "pseudopotentials": get_pseudo_files(atoms),
        }

    # Vibrations
    _atoms = atoms.copy()
    _atoms.calc = calc_obj(**calc_kwargs)

    vib1 = Vibrations(atoms=_atoms, name=f"vib_{name}-{calc_name}_serial")
    vib1.run()
    frq1 = vib1.get_frequencies()

    # ParallelVibrations
    _atoms = atoms.copy()

    vib2 = ParallelVibrations(
        atoms=_atoms,
        name=f"vib_{name}-{calc_name}_parallel",
        calc_obj=calc_obj,
        calc_kwargs=calc_kwargs,
    )
    n = vib2.map_length
    for i in range(1, n + 1):
        vib2.run_tag(i)
    vib2.run()
    frq2 = vib2.get_frequencies()

    # Comparison
    print("Frequency comparison:")
    for f1, f2 in zip(frq1, frq2):
        print(f1, f2)
    assert np.allclose(frq1, frq2, atol=1e-6)

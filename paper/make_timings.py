import matplotlib.pyplot as plt
from math import ceil

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 3))

# case1
natoms_1 = 20
calc_m = 15
fairuse = 10
data_1 = [natoms_1 * calc_m, calc_m, ceil(natoms_1 / fairuse) * calc_m]

ax1.plot(["Serial", "Parallel", "Batch\n Parallel"], data_1, label="Case 1", marker="o")

# case2
natoms_2 = 64
calc_m = 20
fairuse = 10
data_2 = [natoms_2 * calc_m, calc_m, ceil(natoms_2 / fairuse) * calc_m]

ax2.plot(["Serial", "Parallel", "Batch\n Parallel"], data_2, label="Case 2", marker="o")

ax1.set_ylabel("Time (min.)")
ax1.set_title("Case 1: 20 atoms \nwith 15 minutes per calculation", fontsize=12)
ax2.set_title("Case 2: 64 atoms \nwith 20 minutes per calculation", fontsize=12)

fig.savefig("timings.png", dpi=300, bbox_inches="tight")

# Overview

**What are extensions**

Extesions are post-processor plugins that can modify the workflow and output of a specific calculator

**Why use extensions**

In some calculators, the output is not directly usable for the vibrations or infrared computation. The extension can be used to alter the computation behavior and allow the output to be usable for the computation.

```{note}
An example of this is the `Quantum Espresso` calculator and its dipole moment output for the infrared computation workflow. In `Quantum Epresso`, a dipole moment is only computed for a specific direction but in some cases we need a three-dimensional dipole moment array. The extension can be used to compute the three-dimensional dipole moment array by doing three separate calculations for each direction. The extension handles this automatically.
```

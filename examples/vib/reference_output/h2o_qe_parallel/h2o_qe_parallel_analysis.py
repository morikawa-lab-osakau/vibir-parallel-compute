from ase.build import molecule
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelVibrations
from vibir_parallel_compute.utils.analysis import Analysis
from pathlib import Path
import sys


atoms = molecule("H2O", cell=[10, 10, 10])
atoms.center()

# Set the calculator object
calc_obj = Espresso

# Initialize ParallelVibrations
vib = ParallelVibrations(
    atoms,
    name="H2O_qe_parallel",
    calc_obj=calc_obj,
)

# Collect the data
vib.run()

# Initialize analysis
an = Analysis(vib)

an.get_summary()

an.export_traj()

an.export_gif(parallel=4)

an.get_spectra_plot(save="spectra_h2O_qe.png")

#! /bin/sh

set -x

VENV_PATH="/home/testuser/pyenv/bin/activate"

# Activate the virtual environment
source $VENV_PATH

# Execute the given command (if any)
exec "$@"



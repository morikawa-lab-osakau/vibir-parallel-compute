# Quantum Espresso

Quantum Espresso (QE) is not compatible with the Infrared module. The dipole moment is a required output for the Infrared module, but QE does not provide this readily. First, you need to apply a 0 magnitude electric field to the system to run the dipole moment computation along the electric field direction. This means you only get *one* dipole moment value per direction per calculation. This is okay if you only need one direction (i.e. surfaces) but not for the full 3D dipole moment arrays (i.e. molecules). This is a limitation of QE and not the Infrared module. 

The solution to this is to compute the dipole moment along the needed directions and compile the outputs into a single array. This extension does these automatically.

## **Input Data defaults**

The following default values will be added to the `input_data` dictionary in the `Espresso` calculator object. 

```python
defaults = {
    "tefield": True,
    "dipfield": True,
    "emaxpos": float((self.atoms.get_center_of_mass(scaled=True)[manual_direction] + 0.5) % 1),
    "eopreg": 0.0001,
    "eamp": 0.0,
    "edir": int(manual_direction + 1),
}
for key, value in defaults.items():
    input_data.setdefault(key, value)
```

## **Workflow**

The workflow is simple. 

1. It will take the `direction` variable: to know which directions to calculate
2. Loop through each direction
   1. Compute dipole moment along the direction
3. Compile the dipole moment arrays into a single array
4. Pass the dipole moment array to the Infrared module

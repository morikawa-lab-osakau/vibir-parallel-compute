# VibIR-Parallel-Compute

This packages is primarily about doing Vibration and Infrared computations and analysis. It is designed to be conducted in parallel and take advantage of multiple nodes available in High Performance Computing (HPC) environments. Aside from the primary computations, additional tools are provided to help with the analysis and post-processing.

## Getting started

### Installation

**via direct Pip**

Activate your python environment (`conda` or `virtualenv`) and install using `pip`:

```bash
pip install git+https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
```

```{note}
This will install the latest version
```

**via Git**

Download the `git` repository and install:

```bash
git clone https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
pip install vibir-parallel-compute/
```

This method allows you to retain a copy of the code and also to select a specific version you need. 

**Developer mode**

If you want to add new features or need to edit some portion of the code. You can install it using developer mode to save time on frequent reinstalls.

```bash
git clone https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
pip install -e vibir-parallel-compute/
```

### Usage

A web manual is made available in https://morikawa-lab-osakau.gitlab.io/vibir-parallel-compute. Tutorials and examples are provided to help you get started.


### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

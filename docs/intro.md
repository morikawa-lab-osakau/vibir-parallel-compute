# Welcome to VibIR Parallel Compute

## What is VibIR Parallel Compute?

This packages is primarily about doing **Vibration** and **Infrared** computations and analysis. It is designed to be conducted in parallel and take advantage of multiple nodes available in High Performance Computing (HPC) environments. Aside from the primary computations, additional tools are provided to help with the analysis and post-processing.

## Features

The most important general features are:

1. Parallel computations of displaced structures
2. Post-processing extensions
3. Easy and fast analysis


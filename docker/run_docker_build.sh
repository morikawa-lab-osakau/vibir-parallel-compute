#!/bin/bash

docker build --no-cache -t registry.gitlab.com/morikawa-lab-osakau/vibir-parallel-compute .
docker push registry.gitlab.com/morikawa-lab-osakau/vibir-parallel-compute

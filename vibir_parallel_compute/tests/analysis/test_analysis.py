from ase.calculators.espresso import Espresso
from ase.build import molecule

from vibir_parallel_compute import ParallelVibrations, ParallelInfrared

from vibir_parallel_compute.tests.aseconfig import *
from vibir_parallel_compute.utils.analysis import Analysis
from pathlib import Path

atoms = molecule("H2", cell=[5, 5, 5])
atoms.center()

calc_kwargs = {
    "input_data": input_data_template,
    "profile": espresso_profile,
    "kpts": (1, 1, 1),
    "pseudopotentials": get_pseudo_files(atoms),
}


@pytest.mark.parametrize("calc_type", [("vib", ParallelVibrations), ("ir", ParallelInfrared)])
def test_analysis_initializiation(calc_type):
    name, cls = calc_type
    obj = cls(
        atoms=atoms,
        name=f"{name}_parallel",
        calc_obj=Espresso,
        calc_kwargs=calc_kwargs,
    )
    obj.run()

    an = Analysis(vibir_obj=obj)
    an.get_summary()


@pytest.mark.parametrize("calc_type", [("vib", ParallelVibrations), ("ir", ParallelInfrared)])
def test_analysis_trajexport(calc_type):
    name, cls = calc_type
    obj = cls(
        atoms=atoms,
        name=f"{name}_parallel",
        calc_obj=Espresso,
        calc_kwargs=calc_kwargs,
    )
    obj.run()

    an = Analysis(vibir_obj=obj)
    an.get_summary()

    an.export_traj()
    dirpath = Path(f"vibir_analysis/{cls.__name__}_mode_traj")
    assert dirpath.exists()
    nfiles = len(list(dirpath.glob("*.traj")))
    assert nfiles == 6


@pytest.mark.parametrize("calc_type", [("vib", ParallelVibrations), ("ir", ParallelInfrared)])
def test_analysis_gifexport(calc_type):
    name, cls = calc_type
    obj = cls(
        atoms=atoms,
        name=f"{name}_parallel",
        calc_obj=Espresso,
        calc_kwargs=calc_kwargs,
    )
    obj.run()

    an = Analysis(vibir_obj=obj)
    an.get_summary()

    an.export_gif(parallel=1)
    dirpath = Path(f"vibir_analysis/{cls.__name__}_mode_gif")
    assert dirpath.exists()
    nfiles = len(list(dirpath.glob("*.gif")))
    assert nfiles == 6


@pytest.mark.parametrize("calc_type", [("vib", ParallelVibrations), ("ir", ParallelInfrared)])
def test_analysis_spectraexport(calc_type):
    name, cls = calc_type
    obj = cls(
        atoms=atoms,
        name=f"{name}_parallel",
        calc_obj=Espresso,
        calc_kwargs=calc_kwargs,
    )
    obj.run()

    an = Analysis(vibir_obj=obj)
    an.get_summary()

    fig, ax = an.get_spectra_plot(save=f"spectra_{name}.png")
    fn = Path(f"spectra_{name}.png")
    assert fig is not None
    assert fn.exists()

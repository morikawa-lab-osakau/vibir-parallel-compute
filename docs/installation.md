# Installation

### via direct Pip

Activate your python environment (`conda` or `virtualenv`) and install using `pip`:

```bash
pip install git+https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
```

```{note}
This will install the latest version
```

### via Git

Download the `git` repository and install:

```bash
git clone https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
pip install vibir-parallel-compute/
```

```{note}
This method allows you to retain a copy of the code and also to select a specific version you need. 
```

### Developer mode

If you want to add new features or need to edit some portion of the code. You can install it using developer mode to save time on frequent reinstalls.

```bash
git clone https://gitlab.com/morikawa-lab-osakau/vibir-parallel-compute.git
pip install -e vibir-parallel-compute/
```

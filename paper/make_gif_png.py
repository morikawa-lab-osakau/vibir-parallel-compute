import matplotlib.pyplot as plt
from ase.io import read, write
from vibir_parallel_compute.utils.plot import plot_atoms, plot_cell
from pathlib import Path

fname = Path("mode_6.traj")

fig, axs = plt.subplots(1, 3, figsize=(12, 4))


images = read(fname, ":")


for ax, view in zip(axs, ["xy+", "xz+", "yz+"]):
    # ax.cla()
    n = 0
    rmsd_max = 0
    for i, atom in enumerate(images):
        p1 = images[0].get_positions()
        p2 = atom.get_positions()
        d = p2 - p1
        rmsd = (d**2).sum() ** 0.5
        if rmsd > rmsd_max:
            rmsd_max = rmsd
            n = i

    print(f"rmsd_max: {rmsd_max}", f"idx: {n}")

    for atom, alpha in zip([images[0], images[n]], [0.25, 1]):
        plot_atoms(
            ax=ax,
            atoms=atom,
            plot_constraint=True,
            radius_factor=0.8,
            plane=view,
            patch_kwargs={"alpha": alpha},
        )

    plot_cell(ax=ax, cell=atom.get_cell(), plane=view)

    zoom_window = 3
    center = atom.get_center_of_mass()

    if view == "yz+":
        ylim = (0 - 1, atom.get_cell().sum(axis=0)[2] + 1)
        xlim = (0 - 1, atom.get_cell().sum(axis=0)[1] + 1)
        center = (atom.get_cell().sum(axis=0)[1] / 2, atom.get_cell().sum(axis=0)[2] / 2)
        if zoom_window:
            ylim = (center[1] - zoom_window / 2, center[1] + zoom_window / 2)
            xlim = (center[0] - zoom_window / 2, center[0] + zoom_window / 2)
        ax.set_ylim(*ylim)
        ax.set_xlim(*xlim)
    if view == "xz+":
        ylim = (0 - 1, atom.get_cell().sum(axis=0)[2] + 1)
        xlim = (0 - 1, atom.get_cell().sum(axis=0)[0] + 1)
        center = (atom.get_cell().sum(axis=0)[0] / 2, atom.get_cell().sum(axis=0)[2] / 2)
        if zoom_window:
            ylim = (center[1] - zoom_window / 2, center[1] + zoom_window / 2)
            xlim = (center[0] - zoom_window / 2, center[0] + zoom_window / 2)
        ax.set_xlim(*xlim)
        ax.set_ylim(*ylim)
    if view == "xy+":
        ylim = (0 - 1, atom.get_cell().sum(axis=0)[1] + 1)
        xlim = (0 - 1, atom.get_cell().sum(axis=0)[0] + 1)
        center = (atom.get_cell().sum(axis=0)[0] / 2, atom.get_cell().sum(axis=0)[1] / 2)
        if zoom_window:
            ylim = (center[1] - zoom_window / 2, center[1] + zoom_window / 2)
            xlim = (center[0] - zoom_window / 2, center[0] + zoom_window / 2)
        ax.set_xlim(*xlim)
        ax.set_ylim(*ylim)

fig.savefig(fname.stem + ".png", dpi=300, bbox_inches="tight")

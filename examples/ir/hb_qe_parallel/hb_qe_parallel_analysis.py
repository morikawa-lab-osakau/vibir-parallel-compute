from ase.io import read, write
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelInfrared
from vibir_parallel_compute.utils.analysis import Analysis
from pathlib import Path
import sys


atoms = read("hb_unit.xsf")
atoms.center(axis=2)

# Set the calculator object
calc_obj = Espresso

# Initialize ParallelVibrations
ir = ParallelInfrared(
    atoms,
    name="HB_qe_parallel",
    calc_obj=calc_obj,
    directions=[2],
)

# Collect the data
ir.run()

# Initialize analysis
an = Analysis(ir)

an.get_summary()

an.export_traj()

an.export_gif(parallel=4)

an.get_spectra_plot(save="spectra_h2O_qe.png")

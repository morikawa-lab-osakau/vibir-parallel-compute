from vibir_parallel_compute import ParallelInfrared
from vibir_parallel_compute.tests.aseconfig import *
from ase.optimize import BFGS


@pytest.mark.parametrize("calc_obj", [Espresso])
@pytest.mark.parametrize("name", ["H2", "N2"])
def test_workflow(name, calc_obj):
    atoms = molecule(name, cell=[5, 5, 5])
    calc_name = calc_obj.__name__
    if calc_name == "Espresso":
        calc_kwargs = {
            "input_data": input_data_template,
            "profile": espresso_profile,
            "kpts": (1, 1, 1),
            "pseudopotentials": get_pseudo_files(atoms),
        }

    atoms.calc = calc_obj(**calc_kwargs)
    BFGS(atoms).run(fmax=0.05)

    _atoms = atoms.copy()

    ir2 = ParallelInfrared(
        atoms=_atoms,
        name=f"vib_{name}-{calc_name}_parallel",
        calc_obj=calc_obj,
        calc_kwargs=calc_kwargs,
    )
    n = ir2.map_length
    for i in range(1, n + 1):
        ir2.run_tag(i, cleanfiles=[espresso_tmpdir])
    ir2.run()
    frq2 = ir2.get_frequencies()

    for f2 in frq2:
        print(f2)
    assert frq2 is not None

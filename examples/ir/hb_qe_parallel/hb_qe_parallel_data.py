from ase.io import read, write
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelInfrared
from pathlib import Path
import sys


atoms = read("hb_unit.xsf")
atoms.center(axis=2)

# Set the calculator object
calc_obj = Espresso

# Set calculator object kwargs
outdir = "TMPDIR"
calc_kwargs = {
    "profile": EspressoProfile(
        command="mpirun pw.x",
        pseudo_dir=Path(".").resolve(),
    ),
    "input_data": {
        "control": {
            "tprnfor": True,
            "outdir": outdir,
        },
        "system": {
            "ecutwfc": 50,
            "ecutrho": 400,
        },
    },
    "pseudopotentials": {
        "H": "h_pbe_v1.4.uspp.F.UPF",
        "B": "b_pbe_v1.4.uspp.F.UPF",
    },
    "directory": "DFT",
    "kpts": (16, 16, 1),
}


# Initialize ParallelVibrations
ir = ParallelInfrared(
    atoms,
    name="HB_qe_parallel",
    calc_obj=calc_obj,
    calc_kwargs=calc_kwargs,
    directions=[2],
)

# Run a single displacement (Parallelization is through job arrays)
ARRAY_ID = int(sys.argv[1])
ir.run_tag(ARRAY_ID, cleanfiles=[outdir])

# `Analysis`

### Overview

Analysis is a helpful utility class to quickly analyze the vibrations and infrared data, providing easy to interpret data. 

There are 4 features:
1. `get_summary()` - Get the tabulated summary of vibrational and infrared frequencies (if available) and outputs it to prompt or file
2. 


### Class specifics

```python
class Analysis:
    def __init__(self, 
                 vibir_obj: Union[ParallelVibrations, ParallelInfrared], 
                 cache: Union[Path, str] = "vibir_analysis"):
```

**Parameters**

- `vibir_obj` (Union[`ParallelVibrations`, `ParallelInfrared`])
  : An object representing the vibrations or infrared data.

- `cache` (Union[`Path`, `str`], optional)
  : The path to the cache directory (the location of all output files). Defaults to "vibir_analysis".

### Methods

#### `get_summary()`

```python
def get_summary(**kwargs):
```

**Parameters**

- `kwargs`
  : passed to `summary` method of ASE.

```{Note}
Accepted `kwargs` and their defaults are
- `method='standard'`
- `direction='central'`
- `freq=None`
- `log=sys.stdout`
```

**Example**

```python
vib = ParallelVibrations(atoms, name="H2_qe_parallel", calc_obj=calc_obj)
an = Analysis(vib)
an.get_summary()
```

```text
---------------------
  #    meV     cm^-1
---------------------
  0  102.6i    827.3i
  1  102.6i    827.3i
  2    0.0i      0.0i
  3    0.0       0.0
  4    0.0       0.0
  5  587.3    4736.7
---------------------
Zero-point energy: 0.294 eV
```

---

#### `export_traj()`

```python
def export_traj(parallel: int = 1, frames: int = 15):
```

**Parameters**

- `parallel` (int, optional)
  : The number of parallel processes to use. Defaults to 1.

- `frames` (int, optional)
  : The number of frames in trajectory to export. Defaults to 15.

**Example**

```python
vib = ParallelVibrations(atoms, name="H2_qe_parallel", calc_obj=calc_obj)
an = Analysis(vib)
an.export_traj()
```

---

#### `export_gif()`

```python
def export_gif(parallel: int = 1, frames: int = 15):
```

**Parameters**

- `parallel` (int, optional)
  : The number of parallel processes to use. Defaults to 1.

- `frames` (int, optional)
  : The number of frames trajectory to use in the animatin (lower frames = faster rendering). Defaults to 15.

**Example**

```python
vib = ParallelVibrations(atoms, name="H2_qe_parallel", calc_obj=calc_obj)
an = Analysis(vib)
an.export_gif()
```

![H2_qe_parallel_mode_5](H2_qe_parallel_mode_5.gif)

![H2O_qe_parallel_mode_8](H2O_qe_parallel_mode_8.gif)







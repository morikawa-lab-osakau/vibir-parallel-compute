from ase.build import molecule
from ase.calculators.emt import EMT
from ase.calculators.espresso import Espresso, EspressoProfile
from ase.vibrations import Vibrations, Infrared
from pathlib import Path
import pytest
import os
import numpy as np

espresso_pseudo = os.environ.get("ESPRESSO_PSEUDO", None)
espresso_command = os.environ.get("ESPRESSO_COMMAND", None)
espresso_profile = EspressoProfile(espresso_command, espresso_pseudo)
espresso_tmpdir = os.environ.get("ESPRESSO_TMPDIR", None)

input_data_template = {
    "tprnfor": True,
    "occupations": "smearing",
    "smearing": "mp",
    "degauss": 0.02,
    "ecutwfc": 30,
    "ecutrho": 240,
}


def get_pseudo_files(atoms):
    species = np.unique(atoms.get_chemical_symbols())
    pseudo_files = {}
    for s in species:
        fname = next(Path(espresso_pseudo).glob(f"{s.lower()}_pbe*.UPF")).name
        pseudo_files[str(s)] = fname
    return pseudo_files

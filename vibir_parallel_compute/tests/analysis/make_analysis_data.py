from ase.build import molecule
from ase.calculators.espresso import Espresso
from vibir_parallel_compute import ParallelVibrations, ParallelInfrared
from vibir_parallel_compute.tests.aseconfig import *
from tqdm import tqdm

atoms = molecule("H2O", cell=[10, 10, 10])
atoms.center()

calc_obj = Espresso
calc_kwargs = {
    "input_data": input_data_template,
    "profile": espresso_profile,
    "pseudopotentials": get_pseudo_files(atoms),
    "directory": "DFT",
}


def run_vib(atoms):
    _atoms = atoms.copy()
    vib = ParallelVibrations(
        atoms=_atoms,
        name="vib_parallel",
        calc_obj=calc_obj,
        calc_kwargs=calc_kwargs,
    )
    n = vib.map_length
    for i in tqdm(list(range(1, n + 1)), desc="Running vib"):
        vib.run_tag(i, cleanfiles=[espresso_tmpdir])
    vib.run()


def run_ir(atoms):
    _atoms = atoms.copy()
    ir = ParallelInfrared(
        atoms=_atoms,
        name="ir_parallel",
        calc_obj=calc_obj,
        calc_kwargs=calc_kwargs,
    )
    n = ir.map_length
    for i in tqdm(list(range(1, n + 1)), desc="Running ir"):
        ir.run_tag(i, cleanfiles=[espresso_tmpdir])
    ir.run()


run_vib(atoms)
run_ir(atoms)

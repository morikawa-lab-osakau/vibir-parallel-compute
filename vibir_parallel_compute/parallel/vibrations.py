import shutil
from ase.vibrations import Vibrations
from ase.io.espresso import Namelist
from pathlib import Path
from typing import Union, List


class ParallelVibrations(Vibrations):

    def __init__(self, atoms, calc_obj, calc_kwargs={}, indices=None, name="vib", delta=0.01, nfree=2):
        super().__init__(atoms, indices, name, delta, nfree)

        self.__name__ = "ParallelVibrations"

        # Initialize ID system for parallel mapping
        self.system_mapping = self.get_system_mapping()
        self.map_length = len(self.system_mapping)

        # Calculator object and kwargs
        self.calc_obj = calc_obj
        self.calc_kwargs = calc_kwargs

    def get_system_mapping(self):
        system_mapping = {key: (disp, image) for key, (disp, image) in enumerate(self.iterdisplace(), start=1)}
        return system_mapping

    def calculate(self, atoms, disp, cleanfiles: List[str] = []):

        # Set calculator
        calc_kwargs = self.calc_kwargs.copy()
        calc_kwargs = self.config_directory(calc_kwargs, disp.name)  # Config directory
        calc_kwargs = self.config_allowforce(calc_kwargs)  # Config allowforce
        calc = self.calc_obj(**calc_kwargs)

        results = {}
        results["forces"] = calc.get_forces(atoms)

        if self.ir:
            results["dipole"] = calc.get_dipole_moment(atoms)

        if cleanfiles:
            self.cleanup(calc_kwargs["directory"], cleanfiles)

        return results

    def run_tag(self, tag, overwrite=True, cleanfiles: List[str] = []):
        # overwrite is used to delete the cache file during reruns
        disp, image = self.system_mapping.get(tag, (None, None))

        # error catch for unknown tag
        tag_error = "".join(
            [
                f"Error: ({tag}) tag has no corresponding mapping",
                "Show valid tags via `vib.system_mapping`.",
            ]
        )
        assert disp is not None, tag_error

        fpath = self.cache._filename(disp.name)

        if overwrite and fpath.exists() and fpath.is_file():
            fpath.unlink()

        with self.cache.lock(disp.name) as handle:
            try:
                result = self.calculate(image, disp, cleanfiles)  # Calculate
                handle.save(result)  # Save
            except AttributeError:
                raise Exception(f"{fpath} exists, delete it and try again.")

    def check_cache(self, tag=0, print_report=True):
        report = []
        true_success = True
        assert isinstance(tag, int), f"Error: tag must be an integer, got {type(tag)} instead."

        def check(key, dispname, report):
            success = True
            fpath = self.cache._filename(dispname)
            if fpath.exists() and fpath.is_file():
                with open(fpath, "r") as f:
                    # if it has content, it is a success
                    f_content = f.read()
                    if f_content:
                        success = success and True
                    else:
                        success = success and False
            else:
                success = success and False
            report.append("Tag: {}, {} - Success: {}".format(key, dispname, success))
            return success

        if tag == 0:
            for key, (disp, image) in self.system_mapping.items():
                s_check = check(key, disp.name, report)

        if tag > 0:
            disp, image = self.system_mapping.get(tag, (None, None))
            s_check = check(tag, disp.name, report)

        report = "\n".join(report)
        if print_report:
            print(report)

        if bool(true_success * s_check):
            return report
        else:
            return true_success

    def config_directory(self, kwargs, name):
        kwargs = kwargs.copy()
        if self.calc_obj.__name__ == "EMT":
            return kwargs

        primary_dir = kwargs.get("directory", ".")
        kwargs["directory"] = "{}/{}".format(primary_dir, name)

        return kwargs

    def config_allowforce(self, kwargs):
        kwargs = kwargs.copy()

        # Espresso-case
        if self.calc_obj.__name__ == "Espresso":
            input_data = Namelist(kwargs.pop("input_data", None))
            input_data.to_nested("pw")
            input_data["control"].setdefault("tprnfor", True)
            kwargs["input_data"] = input_data

        return kwargs

    @staticmethod
    def cleanup(workdir: Union[Path, str], cleanfiles: List[str] = []):
        """
        Cleans up files and directories in the working directory.

        Parameters
        ----------
        workdir : Union[Path, str]
            Working directory to cleanup.
        cleanfiles : List[str]
            List of file patterns to cleanup.

        Returns
        -------
        """

        # Pattern recognition (automatically checks for patterns)
        workdir = Path(workdir)
        files = []
        for f in cleanfiles:
            files += list(workdir.glob(f))

        # Cleanup workflow
        for fpath in files:
            print(f"Cleaning up {fpath}: ", end="")

            if not fpath.exists():
                print("File/Directory not found. Skipping ...")
                continue

            if fpath.is_dir():
                shutil.rmtree(fpath)

            if fpath.is_file():
                fpath.unlink()
            print("Done.")
            continue

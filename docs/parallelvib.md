# `ParallelVibrations`

## Overview

The `ParallelVibrations` is designed to be a **drop-in** replacement for ASE's `Vibrations` module. 

There are two steps in a calculation workflow:

1. Parallel Energy & Force calculation of displaced structures
2. Data collection 


Each of these steps has its own python script and Batch Job script.

```{admonition} More information
:class: tip
The associated files for this example can be found in `examples/vib/h2_qe_parallel`. 
```

### Class specifics


```python
class ParallelVibrations:
    def __init__(self, 
                 atoms: Atoms, 
                 calc_obj: Type[ASE.Calculator], 
                 calc_kwargs: Dict[str, Any] = {}, 
                 indices: List[int] = None, 
                 name: str = "vib", 
                 delta: float = 0.01, 
                 nfree: int = 2):
```

**Parameters**

- `atoms` (ASE `Atoms`)
    : The Atoms object representing the system.
- `calc_obj` (Type[ASE `Calculator`])
    : The calculator class to be used for the calculation (e.g. `EMT`, `Espresso`, `VASP`).
- `calc_kwargs` (Dict[str, Any], optional)
    : The kwargs to be passed to the calculator object. Defaults uses ASE calculator class-specific defaults.
- `indices` (List[int], optional)
    : The indices of the atoms to be displaced. Defaults to all atoms.
- `name` (str)
    : The name to use for files.
- `delta` (float, optional)
    : The displacement distance in Angstroms. Defaults to 0.01.
- `nfree` (int, optional)
    : The number of displacements per degree of freedom per atom, 2 or 4 are supported. Defaults to 2.
---

## Tutorial


### Step 1: Parallel calculations

```{note}
In this example, we use an H2 molecule ($N_{atoms}=2$) in a large box. This means (with `nfree=2` [default]), the total number of displaced structures is $N_{displaced-structures}=1 + (N_{atoms} \times 6) = 13$.

In this case, the displaced structures will be mapped to IDs from 1-13. We use this ID mapping to allocate a specific displaced structure on each parallelized job. 
```

#### Python Script

```{admonition} Look out!
:class: danger
The parallelized nature of this calculation makes multiple temporary files which are typically large. We have a need to delete these files when finished. This can be done by the package automatically by `vib.run_tag(ARRAY_ID, cleanfiles=[outdir])`. Specifically, `cleanfiles` will accept a list of the names of files to be deleted (files, directories and `*` patterns).
```


```python
from ase.build import molecule
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelVibrations
from pathlib import Path
import sys

# System
atoms = molecule("H2", cell=[10, 10, 10])
atoms.center()

# Set the calculator object
calc_obj = Espresso

# Set calculator object kwargs
outdir = "TMPDIR"
calc_kwargs = {
    "profile": EspressoProfile(command="mpirun pw.x", pseudo_dir=Path(".").resolve()),
    "input_data": {"control": {"tprnfor": True, "outdir": outdir},
                   "system": {"ecutwfc": 50, "ecutrho": 400}},
    "pseudopotentials": {"H": "h_pbe_v1.4.uspp.F.UPF"},
    "directory": "DFT",
}

# Initialize ParallelVibrations
vib = ParallelVibrations(atoms, name="H2_qe_parallel",
                         calc_obj=calc_obj, calc_kwargs=calc_kwargs)

# Run a single displacement (Parallelization is through job arrays)
ARRAY_ID = int(sys.argv[1])
vib.run_tag(ARRAY_ID, cleanfiles=[outdir])
```

#### Batch Job script

```{note}
The displaced structure mapping is done using job arrays. We set the range in `#PBS -t 1-13` and pass it to the python script in `python -u h2_qe_parallel_data.py ${PBS_SUBREQNO}`.

Each Job will create the `cache` specific displacement cache file for the Vibrations calculation. 
```

```{admonition} Changes required
:class: danger
This Batch Job script is specific to our testing supercomputer system. You should change it according to your supercomputer environment. It should work with popular Job schedulers since we tested it with SGE, PBS, and SLURM. 
```


```bash
#!/bin/bash
#PBS -q DBG
#PBS -N h2_qe_parallel_data
#PBS --group=G15577
#PBS -l elapstim_req=00:10:00
#PBS -l cpunum_job=76
#PBS -v OMP_NUM_THREADS=1
#PBS -T intmpi
#PBS -b 1
#PBS -j o
#PBS -t 1-13

cd $PBS_O_WORKDIR

# -- ENVIRONMENT --------------------------------------------------------------
module load BaseCPU/2024
source ~/work/apps/pyenv/vibirdev/bin/activate
ulimit -s unlimited

export PATH=/sqfs/home/u6c301/work/apps/qe/q-e-qe-7.2/bin:$PATH

# -- EXECUTION ----------------------------------------------------------------
echo "========= Job started  at `date` =========="
python -u h2_qe_parallel_data.py ${PBS_SUBREQNO}
echo "========= Job finished at `date` =========="
```

### Step 2: Data Collection

#### Python Script

```{note}
After all necessary data has been produced by **Step 1**, it is ready to analyzed. This step 2, simply runs a collector and analysis on the data. This is done in serial so we only need 1 node. Its fast and we can also run this is non-supercomputer environment. 

The data (cache files) will be read and compiled in the `vib.run()` line. Now the vibrations data is available for further analysis.

Lastly, the `Analysis` module was used to provide the spectra and other information. 
```

```{admonition} More information
:class: tip
More information of the `Analysis` module features are available in its own section. 
```


```python
from ase.build import molecule
from ase.calculators.espresso import Espresso, EspressoProfile
from vibir_parallel_compute import ParallelVibrations
from vibir_parallel_compute.utils.analysis import Analysis
from pathlib import Path
import sys

# System
atoms = molecule("H2", cell=[10, 10, 10])
atoms.center()

# Set the calculator object
calc_obj = Espresso

# Initialize ParallelVibrations
vib = ParallelVibrations(atoms, name="H2_qe_parallel", calc_obj=calc_obj) 

# Collect the data
vib.run()

# Initialize analysis
an = Analysis(vib)
an.get_summary()
an.export_traj()
an.export_gif(parallel=4)
an.get_spectra_plot(save="spectra_h2_qe.png")
```

#### Batch Job script

```bash
#!/bin/bash
#PBS -q DBG
#PBS -N h2_qe_parallel_analysis
#PBS --group=G15577
#PBS -l elapstim_req=00:10:00
#PBS -l cpunum_job=76
#PBS -v OMP_NUM_THREADS=1
#PBS -T intmpi
#PBS -b 1
#PBS -j o

cd $PBS_O_WORKDIR

# -- ENVIRONMENT --------------------------------------------------------------
module load BaseCPU/2024
source ~/work/apps/pyenv/vibirdev/bin/activate
ulimit -s unlimited

export PATH=/sqfs/home/u6c301/work/apps/qe/q-e-qe-7.2/bin:$PATH

# -- EXECUTION ----------------------------------------------------------------
echo "========= Job started  at `date` =========="
python -u h2_qe_parallel_analysis.py
echo "========= Job finished at `date` =========="
```
